.. index::
   pair: 2024-02-17 ; Ghadir Hani
   pair: 2024-02-17 ; Yael Braudo-Bahat
   pair: 2024-02-17 ; Vivian Silver

.. wwp_2024_02_17:

==========================================================================
2024-02-17 **6 heures du soir après la guerre, c'est MAINTENANT** |wwp|
==========================================================================

.. _para_wwp_guerre:

Women Wage Peace et la guerre
====================================

Après quatre longs mois de guerre, où en sommes-nous ?

Les Israéliens continuent de revivre la terreur du 7 octobre 2023 et il
reste encore 134 otages détenus par le Hamas.

Les habitants de Gaza vivent dans un endroit qui ressemble à l’enfer.

Les Palestiniens de Cisjordanie souffrent de violences continues, de
restrictions à leur liberté de mouvement ainsi que de défis pour leur vie
économique.

De nombreux citoyens arabes d’Israël se trouvent tiraillés entre deux camps.

Bien entendu, il n’y a pas de frontières à la déshumanisation qui continue
de croître à cause de la peur, de l’humiliation et du désespoir.

Malgré l’immense douleur que tout le monde est en train de vivre, nous
continuons à œuvrer pour la paix aux côtés des soeurs de notre équivalent
palestinien, les Femmes du Soleil (WOS).
Les yeux grands ouverts et le cœur qui ne peut pas encore guérir, nous
savons très bien que la paix est actuellement un mot à éviter catégoriquement
dans nos sociétés.

Néanmoins, nous restons attachées à cette destination, non seulement comme
seule finalité, mais aussi, comme seule voie qui permettra aux deux peuples
un avenir de cette région.

Le WWP approfondit également ses partenariats avec d’autres organisations
basés en Israël depuis le début de la guerre.
Aux côtés du mouvement Standing Together, nous avons apporté notre soutien
aux rassemblements pour la coexistence, en criant que seule la paix peut
nous apporter la sécurité.
Ces rassemblements ont lieu à Tel Aviv, avec des rassemblements plus petits
dans le nord d’Israël, notamment à Acre et Carmiel, le deux peuplés de
populations juives et arabes. Les membres du WWP se tiennent également et au
quotidien aux côtés des familles des captifs sur la “Place des Otages”,
comme la place du Musée de Tel Aviv a été rebaptisée.

Sur la Place des Otages


.. _para_6:

6 heures du soir après la guerre, c'est MAINTENANT
======================================================

Quand la guerre sera finie, rencontrons-nous”, dit le Bon Soldat Švejk
dans la satire anti-guerre tchèque homonyme des années 1920.

Son compagnon annonce: “Tous les jours à six heures, vous me trouverez au
pub appelé Le Gobelet à Vin.”
Ce à quoi Švejk répond: ”Alors on se verra après la guerre à six heures du soir.”

En Israël, ce terme fait référence à une question urgente qui doit être
résolue immédiatement après une guerre.
**Mais WWP comprend que 6 heures du soir après la guerre, c'est MAINTENANT.**

Comme l’ont clairement indiqué les dirigeants de l’armée, nous, Israéliens,
avons besoin d’un horizon politique, même si le gouvernement choisit de
poursuivre la guerre. L’attaque meurtrière du Hamas le 7 octobre 2023 a prouvé
sans aucun doute que la tactique connue sous le nom de “gestion du conflit”
ne fonctionne pas.

Créer un horizon politique est une nécessité absolue pour tous les habitants
de cette région, suivi de négociations pour un nouvel ordre politique,
c'est-à-dire pour une vision, largement attrayante pour les électeurs,
de prospérité et de bien-être soutenue par un plan économique qui augmente
les opportunités.

**Seule la paix apportera la sécurité.**

.. _para_vivian:

Sur ses traces - Vivian Silver |vivian|
==========================================

Cela fait près de quatre mois depuis le décès prématuré de Vivian
Silver, une fervente défenseure de la paix.

Son départ a laissé un vide indescriptible dans nos cœurs.

Alors que nous assistons à une floraison régulière des anémones près de
la frontière de Gaza, les souvenirs de la guerre persistent, les échos
des canons, la présence des soldats, les pierres tombales le long des
routes et les kibboutzim vides.
Ces dures réalités soulignent le conflit en cours, avec 134 Israéliens
toujours retenus captifs à Gaza, incarcérés dans les tunnels du Hamas.

Le 2 février 2024, Vivian aurait fêté ses 75 ans.

Un groupe de membres du WWP et son fils Jonathan ont visité sa tombe à
Be’eri. En ce moment solennel, nous avons ravivé notre engagement envers
son héritage et réaffirmé notre engagement à marcher sur ses traces sur
le chemin de la paix.

"Détournez-vous du mal et faites le bien, recherchez la paix et poursuivez-la." (Psaumes 34:14)

.. youtube:: 4B57VsHwq9Q

.. _para_100:

Les 100 femmes les plus influentes de la BBC en 2023 : Yael Braudo-Bahat |YaelBraudot|
==========================================================================================

- https://www.bbc.co.uk/news/resources/idt-02d9060e-15dc-426c-bfe0-86a6437e5234
- https://www.bbc.com/news/world-middle-east-67619390


.. figure:: images/yael_braudo_bahat.webp

Le Dr Yael Braudo-Bahat |YaelBraudot|, codirectrice du WWP, a été sélectionnée comme l'une
des 100 femmes inspirantes et influentes par la BBC pour 2023.

La BBC a écrit: “Yael Braudo-Bahat apporte son expérience en droit à un
mouvement pacifiste populaire israélien qui compte plus de 50 000 membres.

Créé en 2014, le WWP cherche une solution politique négociée au conflit
israélo-palestinien, **en mettant l'accent sur le rôle des femmes dans le
processus de paix**.

Au cours des deux dernières années, le WWP a collaboré avec un mouvement
sœur palestinien, les Femmes du Soleil.

Braudo-Bahat dit qu’elle doit beaucoup à son mentor, l’éminente militante
pour la paix et cofondatrice du WWP, Vivian Silver, qui a consacré des
décennies de sa vie à favoriser la compréhension et l’égalité entre Israéliens
et Palestiniens.

|vivian| Silver a été tué lors de l’attaque du Hamas le 7 octobre 2023.”

En lire plus sur `la BBC <https://www.bbc.co.uk/news/resources/idt-02d9060e-15dc-426c-bfe0-86a6437e5234>`_

Lire `l'entretien <https://www.bbc.com/news/world-middle-east-67619390>`_


.. _para_nobel_paix:

Une nomination pour le prix Nobel de la paix
==================================================

- https://ecopeaceme.org/
- https://www.jpost.com/israel-news/article-781182 (Three Israeli groups nominated for Nobel Peace prize amid Hamas war)

L'Université Libre d'Amsterdam (Vrije Universiteit Amsterdam), reconnue par
le comité Nobel comme nominateur qualifié de candidats au prix Nobel de
la paix, a proposé le WWP et notre mouvement partenaire palestinien, WOS,
comme nominés pour le prix Nobel de la paix 2024.

L'université est l'une des institutions académiques les plus prestigieuses
d'Amsterdam.

Dans sa lettre de recommandation, l’université a écrit que “leur travail est,
en ce moment, tragiquement important, en considération aussi de leur capacité
à continuer à construire des liens pacifiques entre les communautés palestiniennes et
israéliennes, et cela, malgré les incroyables obstacles auxquels elles ont
été confrontées au cours des deux derniers mois”.

Aux côtés de nos deux mouvements de femmes, l'université a également nominé
EcoPeace Middle East.

Publication dans `le Jerusalem Post <https://www.jpost.com/israel-news/article-781182>`_

.. _para_soutien:

Comment rejoindre un groupe de supporters à l'étranger
==============================================================

- https://www.womenwagepeace.org.il/en/international-support/

Nous avons reçu des centaines de messages de soutien du monde entier
depuis le 7 octobre 2023 et souhaitons vous remercier pour vos chaleureux
mots d'encouragement et votre empathie.

Vos nouvelles ont été très importantes pour nous en ces jours tristes.

Vous êtes nombreux à nous demander comment vous pouvez nous soutenir,
au-delà d’un don. Comme vous l’avez lu ci-dessus, nous continuons à faire
entendre notre voix, en transmettant le même message que celui que nous avons
lancé en 2014, au lendemain de la guerre de 50 jours à Gaza: l’Opération
“Bordure Protectrice”.

Nous exigeons des négociations politiques, **avec une représentation égale
de femmes dans tous les aspects du rétablissement de la paix**.

Ce sont en fin de compte le seul moyen de mettre fin au conflit et de
créer de la sûreté, de la sécurité et des opportunités pour toutes
les personnes vivantes dans cette région.

Nous sommes actuellement en train de former des groupes de supporters
à l'étranger.

Dans plusieurs pays, des groupes locaux ont déjà été créés.

**Nous recherchons toujours des femmes pour diriger des groupes de supporters
aux États-Unis**.
Ces groupes peuvent faire beaucoup pour faire connaître le WWP et notre
partenariat avec WOS.

`En savoir plus..... <https://www.womenwagepeace.org.il/en/international-support/>`_

.. _para_monde:

Autour du monde
===================

L’équipe des relations extérieures du WWP ne se repose pas un instant.

Les relations avec les diplomates développées par Angela Scharf, co-coordinatrice
du WWP au cours des dernières années, combinées au développement de groupes
de soutien à l'étranger, ont désormais porté leurs fruits.

En l’état actuel, un certain nombre d’initiatives sont lancées en Israël
et en Europe, qui joueront sûrement un rôle important dans l’avancement
de nos efforts de consolidation de la paix à l’étranger.

Berlin, Allemagne, décembre 2023
-------------------------------------

- https://www.facebook.com/WomenWagePeace/videos/1523926171795090/?extid=CL-UNK-UNK-UNK-AN_GK0T-GK1C&mibextid=Nif5oz

Un concert-bénéfice de la Philharmonie de Berlin a eu lieu sous le titre
“Ensemble pour l’humanité”, appelant à la libération des otages détenus
à Gaza et à la protection des civils tant du côté palestinien qu’israélien.

Regardez `Meera Eilabouni qui a parlé au nom du WWP et du WOS <https://www.facebook.com/WomenWagePeace/videos/1523926171795090/?extid=CL-UNK-UNK-UNK-AN_GK0T-GK1C&mibextid=Nif5oz>`_.


.. _hanna_2024_01_29:

2024-01-29 Paris, France, janvier 2024 avec Hanna Assouline |hanna| et les "Guerrières de la paix" |guerrieres|
------------------------------------------------------------------------------------------------------------------------

- :ref:`guerrieres:hanna_assouline`
- https://www.jpost.com/israel-news/article-784180

:ref:`Hana Assouline <antisem:hanna_assouline>` est la créatrice du film
“Guerrières de la Paix” sur le WWP et la fondatrice d'un mouvement de femmes
français du même nom, “Les Guerrières de la Paix”.
Elle a projeté son film au Parlement français à Paris, lors d'un événement
organisé par la maire de Paris, Anne Hidalgo.

L'ambassadeur de Suède en Israël, Erik Ullenhag
---------------------------------------------------

L'ambassadeur de Suède en Israël, Erik Ullenhag organise début février
dans sa résidence une table ronde pour les représentants du WWP et du WOS
et les ambassadeurs de divers pays.

L'ambassadeur de Slovénie en Israël
---------------------------------------

L'ambassadeur de Slovénie en Israël Andreja Purkart Martinez, lance plusieurs
initiatives pour le WWP et le WOS.

Le 8 mars 2024, des représentants du WWP et du WOS sont invités au Parlement slovène,
une université de premier plan, et rencontrent de grands médias.
D'autres événements sont en préparation.

L'ambassadeur chypriote en Israël, Kornelios S. Korneliou
-------------------------------------------------------------

L'ambassadeur chypriote en Israël, Kornelios S. Korneliou, lance une réunion
des représentants du WWP et du WOS avec le président du Parlement chypriote
à la mi-avril 2024.

Bruxelles, Belgique
-----------------------

Bruxelles, Belgique: divers événements sont prévus pour la communauté
internationale, le Parlement européen, le Parlement belge, avec la participation
de dignitaires juifs et musulmans en avril.


Vintimille, Italie, 21 avril 2024
---------------------------------------

Vintimille, Italie, 21 avril 2024: l'École de la Paix (Scuola di Pace) de Vintimille
décerne le prix Témoin de Paix au WWP et au WOS.

Vienne, Autriche, 6 mai 2024
---------------------------------

Vienne, Autriche, 6 mai 2024. Des représentants du WWP et du WOS ont été
invités à prononcer un “symposium sur la paix” dans le cadre d'une
exposition “Paix” au Musée juif.

Ceci est organisé en conjonction avec le Séminaire mondial de Salzbourg.

Paris, Berlin, Tel Aviv

.. _para_liens:
.. _vivian_2024_02_17:

2024-02-17 **Liens à lire et à regarder, Ghadir Hani et hommage à Vivian Silver**
====================================================================================

Vivian Silver, Militante Tuée Lors d'Une Attaque du Hamas, Est Commémorée Comme Artisane de la Paix au Mémorial de Winnipeg
---------------------------------------------------------------------------------------------------------------------------------

- https://www.cbc.ca/news/canada/manitoba/vivian-silver-winnipeg-activist-killed-hamas-1.7059642

|vivian| Vivian Silver, Militante Tuée Lors d'Une Attaque du Hamas, Est Commémorée
Comme Artisane de la Paix au Mémorial de Winnipeg - CBC News

La Démocratie Maintenant: à la Mémoire de Vivian Silver, Militante Israélo-Canadienne pour la Paix
------------------------------------------------------------------------------------------------------

La Démocratie Maintenant: à la Mémoire de Vivian Silver, Militante
Israélo-Canadienne pour la Paix Tuée dans une Attaque du Hamas - Democracy Now

.. youtube:: iIKkQlo_XfA

Une déclaration du Lobby européen des Femmes – 4 décembre 2023, **Stopper la Violence Barbare et Commencer à Écouter les Femmes**
-----------------------------------------------------------------------------------------------------------------------------------

- https://womenlobby.org/Stop-Barbaric-Violence-and-Start-Listening-to-Women?lang=en
- https://www.youtube.com/@EuropeanWomenLobby/videos

Une déclaration du Lobby européen des Femmes – 4 décembre 2023, Stopper
la Violence Barbare et Commencer à Écouter les Femmes


Service Commémoratif de la Militante Pacifiste Israélienne-Canadienne Vivian Silver
------------------------------------------------------------------------------------------

- https://www.washingtonpost.com/photography/interactive/2023/vivian-silver-funeral/

En photos: `Service Commémoratif de la Militante Pacifiste Israélienne-Canadienne Vivian Silver <https://www.washingtonpost.com/photography/interactive/2023/vivian-silver-funeral/>`_

Ghadir Hani, membre de Women Wage Peace, présenté danse The Times of Israel
-----------------------------------------------------------------------------------

- https://blogs.timesofisrael.com/my-heart-is-big-enough-for-noa-and-for-musa/

`Ghadir Hani, membre de Women Wage Peace, présenté danse The Times of Israel <https://blogs.timesofisrael.com/my-heart-is-big-enough-for-noa-and-for-musa/>`_



