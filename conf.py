# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Womenwagepeace"
html_title = project

author = "Noam"
html_logo = "images/guerrieres_de_la_paix_100.png"
html_favicon = "images/guerrieres_de_la_paix_100.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]
extensions += ["sphinx.ext.intersphinx"]
extensions += ["sphinxcontrib.youtube"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx_design")

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "bokertov": ("https://judaism.gitlab.io/bokertov/", None),
    "judaisme": ("https://judaism.gitlab.io/judaisme/", None),
    "chinsky": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "iran_2023": ("https://iran.frama.io/luttes-2023/", None),
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "jjr": ("http://jjr.frama.io/juivesetjuifsrevolutionnaires", None),
    "guerrieres": (" https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix", None),
    "raar_2024": ("https://antiracisme.frama.io/infos-2024/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://luttes.frama.io/pour/la-paix/womenwagepeace",
    "repo_url": "https://framagit.org/luttes/pour/la-paix/womenwagepeace",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "indigo",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "master_doc": False,
    "nav_title": f"{project} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://www.lesguerrieresdelapaix.com/",
            "internal": False,
            "title": "Les guerrières de la Paix",
        },
        {
            "href": "https://judaism.gitlab.io/floriane_chinsky/",
            "internal": False,
            "title": "Rav Floriane Chinksy",
        },
        {
            "href": "https://www.womenwagepeace.org.il/en/",
            "internal": False,
            "title": "Women Wage Peace (WWP)",
        },
        {
            "href": "https://luttes.frama.io/pour/la-paix/linkertree/",
            "internal": False,
            "title": "Liens Paix",
        },
        {
            "href": "https://linkertree.frama.io/judaisme/",
            "internal": False,
            "title": "Liens Judaisme",
        },
    ],
    "heroes": {
        "index": "WomenWagePeace",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"2023-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |guerrieres| image:: /images/guerrieres_de_la_paix_avatar.png
.. |paix| image:: /images/guerrieres_de_la_paix_avatar.png
.. |hanna| image:: /images/hanna_assouline_g_avatar.png
.. |floriane| image:: /images/floriane_chinsky_avatar.jpg
.. |wwp| image:: /images/wwp_avatar.webp
.. |YaelBraudot| image:: /images/yael_braudo_bahat_avatar.webp
.. |vivian| image:: /images/vivian_avatar.png
.. |YonathanZeigen| image:: /images/yonathan_zeigen.webp
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
