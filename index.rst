

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

.. un·e
.. ❤️💛💚

.. https://framapiaf.org/web/tags/viviansilver.rss
.. https://framapiaf.org/web/tags/womenwagepeace.rss

|FluxWeb| `RSS <https://luttes.frama.io/pour/la-paix/womenwagepeace/rss.xml>`_

.. _women_wage_peace:
.. _womenwagepeace:

===============================================================================
 |wwp|  💚 **Women Wage Peace (WWP)** |vivian|
===============================================================================

- https://en.wikipedia.org/wiki/Women_Wage_Peace
- https://www.womenwagepeace.org.il/en/

- https://rstockm.github.io/mastowall/?hashtags=paix,peace,pace,womenwagepeace&server=https://framapiaf.org

.. toctree::
   :maxdepth: 6

   militantes/militantes
   articles/articles
