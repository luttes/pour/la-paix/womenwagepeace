.. index::
   pair: Militante; Vivian Silver
   ! Vivian Silver

.. _vivian_silver:

==========================================================================
💚 **Vivian Silver** |vivian|
==========================================================================

- https://www.womenwagepeace.org.il/en/vivian-silver-peace/
- https://en.wikipedia.org/wiki/Vivian_Silver
- https://invidious.fdn.fr/watch?v=V8--NUcy4aU
- https://www.tenoua.org/mort-dun-poeme/
- https://www.btselem.org/
- https://b8ofhope.org/road-to-recovery/
- https://www.instagram.com/b8ofhope/
- https://www.projectrozana.org/
- https://www.instagram.com/projectrozanainternational/
- https://x.com/YonatanZeigen (|YonathanZeigen| Son of the late peace activist Vivian Silver, killed Oct 7th 2023)


Introduction
================

**We are heartbroken**. Vivian Silver, one of WWP founders,  was killed in
her home in Kibbutz Be’eri on October 7th.

**This is a terrible loss for her many friends all over the world**.

.. figure:: images/vivian_silver.png


.. figure:: images/vivian_silver_photos.png

   https://www.womenwagepeace.org.il/en/vivian-silver-peace/

Vivian Silver est native de Winnipeg, Manitoba.

Elle est morte assassinée, à l’âge de 74 ans, au kibboutz Be’eri, en Israël,
le 7 octobre 2023.

Elle fut membre fondatrice de Women Wage Peace “Les guerrières de la paix”,
militante de l’égalité de genre, directrice exécutive et co-fondatrice de
l’Institut du Negev – Centre judéo-arabe pour l’égalité, l’émancipation et la
coopération, membre du bureau de l’organisation de défense des droits humains
B’Tselem, membre active de Road to Recovery [le chemin de la guérison] et
Project Rozana qui accompagnent des Gazaouis malades ou blessés dans les
hôpitaux israéliens.

Message de Hanna Assouline
==============================

Le corps de Vivian Silver,lumineuse militante pour la Paix membre
de @WomenWagePeace vient d’être identifié.On la pensait otage à Gaza,
elle a en fait été assassinée le 7 octobre au kibbutz Be’eri.

Nousétions avec elle 4 jours avant sa mort.
Pas de mot pour exprimer notre chagrin


.. figure:: images/vivian_silver_et_hanna_assouline.png


Vivian Silver, peace activist, confirmed dead
================================================

- http://messages.responder.co.il/8014105/573849775/b2b10607aff3105930a95b8c66f43963/?

For 38 long days and nights our beloved Vivian Silver, Peace Wager, was
pronounced missing, presumed kidnapped.

Last night her family received notice that, to our dismay, she was
murdered on October 7, in her home's safe room in Kibbutz Be'eri.

Vivian was a lifelong social activist, working for the advancement of
women and peace.

She was one of the founders of Women Wage Peace and a pivotal member
since 2014.

She also served as Co-Director in AJEEC – Negev Institute for Peace & Economic
Development and was a board member of "Betzelem".

During her free time, she volunteered to drive Palestinian patients and
their families from the Gaza strip into Israeli hospitals for treatment.

"Enough.

We cannot go on without a political horizon," she said a few years ago,
at a Women Wage Peace rally. "We cannot accept operations and acts of
war that bring only death, destruction and pain, and inflict mental
and physical harm, as a daily occurrence.

We call upon the Prime Minister, the Defense Minister, and the Cabinet
to find the necessary courage to promote political alternatives, which
will bring us peace and security.

We call upon our sisters in Gaza: join us and call upon your leaders,
enough.

Terror benefits no one. You, too, deserve peace and security."

Vivian, in your wisdom and refined sense of humor, you were our role
model and brave leader to peace.
We will not rest until we achieve the goal to which you dedicated your
life's work.

In your life, and in your death, you bequeath us peace.

We join in the deep sorrow of her children and family, and embrace
them (to our hearts).

Our hearts are shattered.
May her memory be a blessing.



Les hommages affluent, après la mort de l’activiste Vivian Silver, tuée dans l’attaque contre un kibboutz
=============================================================================================================

- https://www.middleeasteye.net/fr/actu-et-enquetes/guerre-israel-palestine-deces-activiste-vivian-silver-hommages
- https://www.middleeasteye.net/news/israel-palestine-war-vivian-silver-peace-activist-killed-kibbutz-attack
- https://www.instagram.com/middleeasteye.fr/
- https://www.middleeasteye.net/fr/topics/occupation-palestine


Les hommages affluent, après la mort de l’activiste Vivian Silver,
tuée dans l’attaque contre un kibboutz

Des militants palestiniens et israéliens rendent hommage à l’ancienne
membre du conseil d’administration de Betselem, Vivian Silver, saluant
une femme très engagée, qui a œuvré pour la paix et la justice en
Cisjordanie, à Gaza et auprès des communautés bédouines

Les hommages affluent après l’annonce de la mort de Vivian Silver,
qui fait désormais officiellement partie des victimes de l’attaque
menée par le Hamas le mois dernier contre des localités israéliennes
proches de la bande de Gaza.

L’activiste pour la paix israélo-canadienne de soixante-quatorze ans
habitait Beeri, un kibboutz attaqué par les combattants palestiniens,
Samedi 7 Octobre 2023.

Nous pensions au départ qu’elle faisait partie des deux cent quarante
otages emmenés à Gaza, mais les équipes médico-légales ont confirmé
que ses restes avaient été découverts à Beeri, faisant d’elle
l’une des mille deux cent israéliens tués lors de l’attaque.

La septuagénaire était une activiste pour la paix connue et engagée. Elle
a travaillé avec les palestiniens à Gaza et en Cisjordanie occupée et
avec les bédouins du Néguev en Israël.

Après la guerre israélienne contre Gaza en 2014, elle avait fondé Women
Wage Peace, une organisation de femmes contre l’occupation. Elle avait
en outre cofondé l’Arab Jewish Center for Equality, Empowerment and
Cooperation, dont l’acronyme AJEEC signifie « je viens vers toi »
en arabe.

Elle fut également directrice générale du Negev Institute for Strategies
of Peace and Development, qui encourage la coopération et les partenariats
entre les communautés juives et les communautés arabes, et elle avait
fait partie du conseil d’administration de Betselem, la plus grande
organisation israélienne de défense des droits humains.

Thabet Abu Ras, célèbre universitaire et activiste palestinien au sein
des Abraham Initiatives, confie à Middle East Eye (MEE) que Vivian Silver
était une femme remarquable.

Ils s’étaient rencontrés après le début de la seconde Intifada en
2000 et, avec Amal Elsana Alh Jooj, ils avaient fondé l’AJEEC.

« C’était un effort collectif, mais elle en était le moteur », dit
Thabet Abu Ras, « dès le départ, elle s’est profondément impliquée
dans les projets qui l’ont connectée à Gaza, où elle a noué de
nombreuses amitiés durables. Son engagement pour la paix et les droits
humains était inébranlable ».

La famille de Thabet Abu Ras vient d’Herdibia, un village palestinien
au sud d’Ashkelon dont les habitants ont été chassés par les forces
sionistes lors de la Nakba en 1948.

Il raconte que Vivian Silver était parfaitement consciente de la souffrance
provoquée par l’occupation israélienne de la Cisjordanie et par le
siège imposé sur la bande de Gaza.

« Elle était activement engagée dans les luttes de la communauté
bédouine du Néguev », précise-t-il, ajoutant qu’elle était
particulièrement active depuis dix ans, depuis l’annonce du plan
Prawer, projet de développement controversé qui menaçait de déplacer
soixante-dix mille bédouins de leurs villages dans le désert.

« Obtenir justice, en particulier pour les bédouins, était au cœur de
sa mission. Dans nos discussions avec des juifs de gauche et sionistes,
elle était une alliée indéfectible, soutenant toujours notre cause »,
dit Thabet Abu Ras, « nous partagions souvent mutuellement nos événements
sociaux, reflétant notre respect mutuel et notre collaboration. Sa mort
est une perte importante, en particulier dans le Néguev, où elle était
un phare d’espoir pour les arabes et pour les juifs ».

Thabet Abu Ras indique que les racines de sa famille à Gaza, où beaucoup
ont été chassés en 1948, ont rendu l’absence de Vivian Silver encore
plus poignante, « quand je me remémore l’expérience de la première
Nakba de ma tante de quatre-vingt-trois ans, son parcours d’Herdibia à
Jabaliya, et maintenant sa vie à Khan Younès, je vois la tragédie de
ma tante et je vois Vivian. Je vois les tragédies des deux côtés, les
souffrances inextinguibles des palestiniens et la profonde perte marquée
par la mort de Vivian. Vivian était sensible aux luttes des deux côtés,
israélien et palestinien, faisant écho à notre engagement partagé
pour la paix, pour la justice et pour la reconnaissance des bédouins et
de leurs terres. Sa vision d’une même vie pour les juifs et les arabes
palestiniens reflétait mes propres convictions. Je ressens profondément
sa perte ».

Le fils de Vivian Silver, Yonathan Silver, avait déclaré à MEE, Lundi
9 Octobre 2023, qu’il pensait qu’elle avait été emmenée à Gaza
et qu’il avait eu pour la dernière fois de ses nouvelles le matin de
l’attaque, Samedi 7 Octobre 2023

Plus d’une centaine d’israéliens ont été tués à Beeri, un des
kibboutzim les plus grands de la région. Depuis l’attaque, Israël
mène une opération terrestre et aérienne à Gaza qui a tué onze mille
deux cent palestiniens, dont plus de quatre mille six cent enfants.

L’organisation israélienne de défense des droits humains Betselem,
qui déclarait en 2021 qu’Israël était un régime d’apartheid
appliquant la suprématie juive entre la Méditerranée et le Jourdain,
a fait partie des organisations qui ont rendu hommage à Vivian Silver
en ligne, Mardi 14 Novembre 2023. Vivian Silver avait passé plusieurs
années au conseil d’administration de l’organisation.

David Zonsheine, membre du conseil d’administration de Betselem et ancien
président de l’association, explique à MEE que « c’est une très
mauvaise période pour moi. Mon oncle a été assassiné Samedi 7 Octobre
2023, ma cousine a été emmenée à Gaza, j’ai des amis à Gaza qui ont
perdu leur famille et maintenant, je dois affronter la mort de Vivian ».

Il connaissait Vivian Silver depuis une quinzaine d’années à travers
leur travail pour les droits humains, « j’ai rencontré une femme
très intelligente, sérieuse et engagée. Elle connaissait l’ampleur
de la lutte, non seulement en Cisjordanie ou à Gaza, mais aussi au sein
d’Israël ».

David Zonsheine qualifie Vivian Silver de positive et d’optimiste,
même si elle savait que le chemin était semé d’embûches. Son
implication auprès de Betselem a été essentielle pour faire du
groupe l’organisation respectée pour les droits humains qu’elle
est maintenant.

« Personne n’a échappé à cette guerre. Lorsqu’une population
est attaquée, personne ne demande qui elle prend. Vivian Silver me
manque. Elle était un symbole. Beaucoup de personnes la connaissaient
et elle était impliquée dans diverses luttes. Elle s’est rendue dans
beaucoup d’endroits et elle a essayé de rendre justice. Non seulement
elle croyait en ces sujets, mais elle a fondé des organisations et elle
a agi ».

Thousands Attend Funeral of Slain Canadian-Israeli Peace Activist Vivian Silver
===================================================================================

People from all walks of Israeli society came to say goodbye to Silver,
who was murdered at her home in Kibbutz Be'eri by Hamas on October 7.

'You knew that it doesn’t matter if we speak Hebrew or Arabic, it
doesn’t matter if we were born in the Gaza border or the Gaza Strip –
you knew that our futures are tied together,' said fellow peace activist Ghadir Hani

- https://www.haaretz.com/israel-news/2023-11-16/ty-article/.premium/thousands-attend-funeral-of-slain-canadian-israeli-peace-activist-vivian-silver/0000018b-d9cd-d423-affb-fbef6e360000

Vivian Silver, mort d’un poème
==================================

- https://www.tenoua.org/mort-dun-poeme/
- https://www.instagram.com/p/CznmjBJMi10


Ce soir, j’apprends avec un chagrin infini la mort de Vivian Silver, la mort d’un poème.

Avec une tristesse lourde et un peu de soulagement aussi, il faut l’avouer.
Vivian a été tuée le 7 octobre mais nous ne le savions pas.

Tout le monde pensait qu’elle avait été enlevée et retenue captive à Gaza,
mais les archéologues des cadavres d’Israël l’ont identifiée; Vivian est bien
morte dès le 7 octobre, assassinée chez elle, au kibboutz Be’eri.

Quand je pense à Vivian, j’aime mieux qu’elle n’ait pas vécu la captivité
à Gaza, une terre dont elle s’est évertuée, quoi qu’il en coûte, à protéger
et défendre les habitants.

Vivian Silver Haï, Vivian Silver vit, et c’est elle, son combat, son esprit
poétique et sa lucidité d’esthète de l’humanité qui vaincront.

Que la mémoire de Vivian soit tissée au fil des vivants et qu’elle nous inspire
pour que triomphe sur l’absurde intelligence de la guerre la futile fulgurance
de la poésie.

AJEEC-NISPED is heartbroken and in mourning over the death of our longtime partner and co-founder of the organization, our beloved Vivian Silver, who was murdered in the Hamas terror attack on the Black Saturday of October 7.
======================================================================================================================================================================================================================================

- https://ajeec-nisped.org.il/?page_id=20615&lang=en



AJEEC-NISPED is heartbroken and in mourning over the death of our longtime
partner and co-founder of the organization, our beloved Vivian Silver, who was
murdered in the Hamas terror attack on the Black Saturday of October 7.

Vivian Silver, one of the founders of AJEEC-NISPED, dedicated her life to
social action, to the advancement of women, and to pursuing peace.

She was an activist and did everything in her power in the Knesset and
through various social organizations to promote peace and Arab-Jewish partnership.

In addition to AJEEC, she also founded the “Women Wage Peace” movement, she was
a leading administrator in “B’tselem”, and volunteered in the organization
“On the Way to Healing,” through which she accompanied cancer patients from
the Gaza Strip to hospitals in Israel.

Vivian worked as co-CEO of AJEEC-NISPED for many years, together with her
longtime partner Dr. Amal Elsana-Alh’jooj, and even after completing her work
in that role, she continued to guide the organization and its activities
and was an active board member of the organization until her last day.

AJEEC-NISPED commits to continuing on Vivian’s path, and commemorating her
memory through acting for equal and just Arab-Jewish partnership in every
place that we operate, with the goal to spread light just as she did in her life.

May her memory be a blessing.

Citée en 2024
================

- :ref:`raar_2024:vivian_silver_2024_11_27`
- :ref:`vivian_2024_02_17`

Citée en 2023
================

- :ref:`jjr:vivian_silver_2023_11_24`
